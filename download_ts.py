import os
import sys
import errno
import urllib2
import xmlrpclib
from datetime import datetime, timedelta
import ConfigParser
import logging
from logging.handlers import TimedRotatingFileHandler
import psycopg2
from common import RepeatedTimer

file_xml_rpc_url_template = "http://localhost:{port}/rpc"


def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return int(delta.total_seconds())


def download_file(download_file_url, to_directory, port):
    if not os.path.isdir(to_directory):
        mkdir_p(to_directory)
    file_xml_rpc_url = file_xml_rpc_url_template.format(port=port)
    s = xmlrpclib.ServerProxy(file_xml_rpc_url)
    s.aria2.addUri([download_file_url], {'dir': to_directory})
    logging.info('Downloading task added: ' + download_file_url)


def get_all_downloads(port):
    file_xml_rpc_url = file_xml_rpc_url_template.format(port=port)
    s = xmlrpclib.ServerProxy(file_xml_rpc_url)
    r = s.aria2.tellActive() + s.aria2.tellWaiting(0, 100000)
    return r


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def get_connection_string(conf):
    string = \
        "host=" + conf.get('Database', 'host') + " " + \
        "port=" + conf.get('Database', 'port') + " " + \
        "dbname=" + conf.get('Database', 'db') + " " + \
        "user=" + conf.get('Database', 'username') + " " + \
        "password=" + conf.get('Database', 'password')
    return string


def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="d",
                                       interval=1,
                                       backupCount=5)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


def find_download(url, downloads):
    matches = next((x for x in downloads if x['files'][0]['uris'][0]['uri'] == url), None)
    return True if matches else False


def download_files():
    all_downloads = {}

    try:
        for p in xml_rpc_ports.split(','):
            all_downloads[p] = get_all_downloads(p)
            logging.info(
                'Porqt {port} - currently downloading files: {count}'.format(count=len(all_downloads[p]), port=p))
    except Exception as ex:
        logging.error(ex)
        logging.error('Failed to read from xml rpc')

    for source in sources:

        check_interval = 5

        dt_from = datetime.utcnow() - timedelta(minutes=check_interval)
        dt_to = datetime.utcnow() - timedelta(seconds=120)
        channel_alias = source['channel_alias']
        bitrate = source['bitrate']
        rpc_port = str(source['rpc_port'])

        url = 'http://{ip}/vod/{alias}/{bitrate}/{start}/{end}/playlist.m3u8' \
            .format(ip=shifter_ip,
                    start=unix_time(dt_from),
                    end=unix_time(dt_to),
                    bitrate=bitrate,
                    alias=channel_alias)

        logging.info("Requesting playlist: {url}".format(url=url))

        try:
            response = urllib2.urlopen(url, timeout=5)
            html = response.read()

            for line in html.split('\n'):
                if '.ts' in line:
                    relative_file_name = line[:line.index('?')]
                    file_name = os.path.join(source['directory_in'], bitrate, relative_file_name)
                    ts = int(os.path.splitext(file_name[file_name.rfind('/') + 1:])[0])

                    file_url = ('http://{ip}/{alias}/{bitrate}/' +
                                relative_file_name).format(ip=shifter_ip,
                                                           bitrate=bitrate,
                                                           alias=channel_alias)

                    if rpc_port in all_downloads and find_download(file_url, all_downloads[rpc_port]):
                        logging.info('Already downloading {fn}, skipping'.format(fn=relative_file_name))

                    if rpc_port in all_downloads and not find_download(file_url, all_downloads[rpc_port]) and not os.path.isfile(file_name):
                        download_file(file_url, os.path.dirname(file_name), rpc_port)
                        delta = source['last_downloaded'] > 0 and ts - source['last_downloaded']
                        # logging.info("Time difference with previous chunk: {delta}".format(delta=delta))

                        if max_ts_delta < delta < 300:

                            logging.error(
                                "{channel_alias}-{bitrate} time difference with previous chunk is {delta}s, maximum allowed is "
                                "{max_ts_delta}s, trying to download possibly missing chunks"
                                    .format(channel_alias=channel_alias, bitrate=bitrate, delta=delta,
                                            max_ts_delta=max_ts_delta))

                            for x in range(source['last_downloaded'] + 1, ts):
                                relative_file_name = relative_file_name[:relative_file_name.rfind('/') + 1] + str(
                                    x) + '.ts'
                                file_name = os.path.join(source['directory_in'], bitrate, relative_file_name)
                                file_url = ('http://{ip}/{alias}/{bitrate}/' +
                                            relative_file_name).format(ip=shifter_ip, bitrate=bitrate,
                                                                       alias=source['channel_alias'])

                                if not find_download(file_url, all_downloads[rpc_port]) and not os.path.isfile(
                                        file_name):
                                    download_file(file_url, os.path.dirname(file_name), rpc_port)

                        source['last_downloaded'] = ts

        except urllib2.HTTPError, error:
            logging.error(error.url)
            logging.error("Error " + format(error))

        except Exception as error:
            logging.error("Error " + format(error))
            sys.exit()


# Starting....
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, 'tasks.cfg')

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)
except:
    sys.exit()

log_path = config.get('Logging', 'path')
if not log_path:
    log_path = current_path

log_file = os.path.join(log_path, 'mtvl-publ-download-ts.log')
logging = create_timed_rotating_log(log_file)
logging.info('Task started')

logging.info('Config read')

connection_string = get_connection_string(config)

logging.info("Connecting to Postgresql DB")

# Connecting to PG server
try:
    conn = psycopg2.connect(connection_string)
except Exception as e:
    logging.error("Error connecting to db " + format(e))
    sys.exit()

cur = conn.cursor()
logging.info('Connected to Postgresql, ' +
             'host:' + config.get('Database', 'host') +
             ' db:' + config.get('Database', 'db'))

try:
    cur.execute("select code, directory_in, bitrates_source, rpc_port \
        from sources \
        where active")

    rows = cur.fetchall()

except Exception as e:
    logging.error("Error receiving data from db " + format(e))
    sys.exit()

logging.info('Sources found: {count}'.format(count=len(rows)))

sources = []

for row in rows:

    channel_alias = row[0]
    directory_in = row[1]
    bitrates_in_source = row[2]
    rpc_port = row[3]

    for bitrate in bitrates_in_source.split(','):
        sources.append({'channel_alias': channel_alias,
                        'directory_in': directory_in,
                        'bitrate': bitrate,
                        'rpc_port': rpc_port,
                        'last_downloaded': 0})
        logging.info('Source: {channel_alias} {bitrate}'.format(channel_alias=channel_alias, bitrate=bitrate))

cur.close()
conn.close()

xml_rpc_ports = config.get('Download', 'xml_rpc_ports')
shifter_ip = config.get('Download', 'shifter_ip')
max_ts_delta = int(config.get('Work', 'max_ts_delta'))

logging.info('Download->xml_rpc_ports: {param}'.format(param=xml_rpc_ports))
logging.info('Download->shifter_ip: {param}'.format(param=shifter_ip))
logging.info('Work->max_ts_delta: {param}'.format(param=max_ts_delta))

rt = RepeatedTimer(10, download_files)
