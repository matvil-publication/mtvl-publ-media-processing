import os
import sys
import urllib2
import xmlrpclib
import ConfigParser
import logging
import socket
from common import RepeatedTimer, create_timed_rotating_log, mtvl_send_mail


file_xml_rpc_url_template = "http://localhost:{port}/rpc"


def get_all_downloads(port):
    file_xml_rpc_url = file_xml_rpc_url_template.format(port=port)
    s = xmlrpclib.ServerProxy(file_xml_rpc_url)
    r = s.aria2.tellActive() + s.aria2.tellWaiting(0, 100000)
    return r


def check_downloads():

    is_error = False
    error_msg = ""

    try:
        for p in xml_rpc_ports.split(','):
            all_downloads = get_all_downloads(p)
            s = "Port {port} - queue size: {count}".format(count=len(all_downloads), port=p)
            logging.info(s)
            if len(all_downloads) > warning_min_queue_size:
                is_error = True
                error_msg += "{msg}\n".format(msg=s)

    except urllib2.HTTPError, error:
        s = "Error: {msg}".format(msg=error.url)
        logging.error(s)
        is_error = True
        error_msg += "{msg}\n".format(msg=s)

    except Exception as ex:
        s = "Error: {msg}".format(msg=ex.url)
        logging.error(s)
        is_error = True
        error_msg += "{msg}\n".format(msg=s)

    if is_error:
        host_name = socket.gethostname()
        send_to = config.get('Mail', 'send_to_admins')
        mtvl_send_mail('Quick publication download QUEUE TOO LARGE on {host_name}'.format(host_name=host_name), error_msg, send_to)
        logging.info("Message sent to {emails}".format(emails=send_to))


# Starting....
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, 'tasks.cfg')

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)
except Exception as error:
    sys.exit()

log_path = config.get('Logging', 'path')
if not log_path:
    log_path = current_path

log_file = os.path.join(log_path, 'mtvl-publ-download-monitor.log')
logging = create_timed_rotating_log(log_file)

logging.info('Task started')
logging.info('Config read')

xml_rpc_ports = config.get('Download', 'xml_rpc_ports')
interval = int(config.get('Download', 'check_task_interval'))
warning_min_queue_size = int(config.get('Download', 'check_task_warning_min_queue_size'))

rt = RepeatedTimer(interval, check_downloads)
