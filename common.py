from threading import Timer
import os
import ConfigParser
import logging
from logging.handlers import TimedRotatingFileHandler


def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="d",
                                       interval=1,
                                       backupCount=7)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.stop()
        self.function(*self.args, **self.kwargs)
        self.start()

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


def mtvl_send_mail(subject, body, send_to):

    # Import smtplib for the actual sending function
    import smtplib

    # Import the email modules we'll need
    from email.mime.text import MIMEText

    msg = MIMEText(body)
    msg['Subject'] = subject

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    config = read_config('tasks.cfg')

    send_from = config.get('Mail', 'send_from')

    msg['From'] = send_from
    msg['To'] = send_to

    recipients = []
    for r in send_to.split(','):
        recipients.append(r)

    smtp_server = config.get('Mail', 'smtp_server')
    smtp_userid = config.get('Mail', 'smtp_userid')
    smtp_password = config.get('Mail', 'smtp_password')

    s = smtplib.SMTP(smtp_server)
    s.login(smtp_userid, smtp_password)

    s.sendmail(send_from, recipients, msg.as_string())
    s.quit()


def read_config(file_name):

    current_path = os.path.dirname(os.path.realpath(__file__))
    config_file = os.path.join(current_path, file_name)

    try:
        config = ConfigParser.ConfigParser()
        config.read(config_file)
    except:
        print "Cannot load " + config_file
        config = None

    return config