from logging.handlers import TimedRotatingFileHandler
from decimal import *
from shutil import copy, rmtree
from psycopg2.extensions import adapt
from common import RepeatedTimer, mtvl_send_mail
from datetime import datetime

import json
import psycopg2
import sys
import ConfigParser
import logging
import os
import tempfile
import hashlib
import ntpath
import shutil
import socket
import urllib2


def stream_duration(file_name):
    command = 'ffprobe -v quiet -print_format json -show_streams ' + file_name
    out = os_run_command(command)
    c = json.loads(out)['streams']
    video = 0
    audio = 0
    for x in c:
        codec_type = x['codec_type']
        try:
            if codec_type == 'video':
                video = x['duration']
            elif codec_type == 'audio':
                audio = x['duration']
        except:
            logging.error('Cannot find {type} stream duration'.format(type=codec_type))
    return video, audio


def chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]


def md5_for_file_name(fn):
    f = open(fn, 'rb')
    return md5_for_file(f)


def md5_for_file(f, block_size=2 ** 20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return str(md5.hexdigest())


def os_run_command(command):

    # logging.info(command)

    import subprocess

    try:
        result = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as spe:
        logging.error('')
        logging.error('')
        logging.error('=' * 50)
        logging.error(command)
        logging.error(spe.output)
        logging.error('=' * 50)
        result = False
        logging.error('')
        logging.error('')

    return result


def update_task_status_error(task_id, message):
    # updating status, executing
    print '===' + message
    cur.execute("UPDATE tasks SET status='error', updated_utc=now() at time zone 'utc', try_qty=try_qty+1, message={message} \
        WHERE id={id}".format(message=adapt(message), id=task_id))
    conn.commit()


def update_task_status(id, status):
    # updating status, executing
    cur.execute("UPDATE tasks SET status='{status}', updated_utc=now() at time zone 'utc' WHERE id={id}"
                .format(status=status, id=id))
    conn.commit()


def update_task_duration(id, duration):
    # updating status, executing
    cur.execute("UPDATE tasks SET duration='{d}' WHERE id={id}".format(d=round(duration, 3), id=id))
    conn.commit()


def get_nearest_file(fname):
    if os.path.exists(fname):
        return fname
    timestamp = os.path.splitext(ntpath.basename(fname))[0]
    directory = os.path.dirname(fname)

    for x in range(-5, 6):
        fnext = os.path.join(directory, str(Decimal(timestamp) + x) + ".ts")
        if os.path.exists(fnext):
            return fnext
    return ""


class CustomExceptiom(Exception):
    def __init__(self, m):
        self.message = m

    def __str__(self):
        return self.message


def get_connection_string(config):
    string = \
        "host=" + config.get('Database', 'host') + " " + \
        "port=" + config.get('Database', 'port') + " " + \
        "dbname=" + config.get('Database', 'db') + " " + \
        "user=" + config.get('Database', 'username') + " " + \
        "password=" + config.get('Database', 'password')
    return string


def create_timed_rotating_log(path):
    """"""
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(path,
                                       when="d",
                                       interval=1,
                                       backupCount=7)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


def process_task(task_row):
    import time

    id = task_row[0]

    media_object_id = task_row[1]
    content = task_row[2]

    directory_in = task_row[3]
    directory_out_mp4 = task_row[4]
    directory_out_wmv = task_row[5]
    bitrates_mp4 = task_row[6]
    bitrates_wmv = task_row[7]
    source_code = task_row[8]
    format = task_row[9]

    if bitrates_mp4 and not os.path.isdir(directory_out_mp4):
        logging.error('MP4 output directory does not exist: {dir}'.format(source=source_code, dir=directory_out_mp4))
        return

    if bitrates_wmv and not os.path.isdir(directory_out_wmv):
        logging.error('WMV output directory does not exist: {dir}'.format(source=source_code, dir=directory_out_wmv))
        return

    logging.info('')
    logging.info('{:16s} {:10s}'.format('ID:', str(id)))
    logging.info('{:16s} {:10s}'.format('Media object ID:', str(media_object_id)))
    logging.info('{:16s} {:10s}'.format('Source:', source_code))
    logging.info('{:16s} {:10s}'.format('Format:', format))

    x = json.loads(content)

    import base64

    xml_task = base64.b64decode(x['content'])

    import xml.etree.ElementTree as ET

    root = ET.fromstring(xml_task)

    duration = root.find('TotalDuration').text
    logging.info('{:16s} {:10s}'.format('Duration:', duration))

    title = root.find('Title').text
    logging.info('{:16s} {:16s}'.format('Title:', title))

    air_time = root.find('AirDate').text
    logging.info('{:16s} {:10s}'.format('Air date:', air_time))

    start = time.time()

    tmp_path = os.path.join(tempfile.gettempdir(), 'process_{id}'.format(id=media_object_id))
    output_files = []


    def send_alert(msg):

        #sending alert e-mail

        host_name = socket.gethostname()

        mail_body  = ""
        mail_body += "Task ID: {task_id}\n".format(task_id=id)
        mail_body += "Host: {host_name}\n".format(host_name=host_name)
        mail_body += "Media object ID: {media_object_id}\n".format(media_object_id=media_object_id)
        mail_body += "Date/time (UTC): {dt}\n".format(dt=datetime.utcnow())
        mail_body += "Error description: {message}\n".format(message=msg)

        send_to = config.get('Mail', 'send_to_operators')

        mtvl_send_mail('Quick publication ERROR on {host_name}'.format(host_name=host_name), mail_body, send_to)


    def process_one_bitrate(bitrate, output_file_name, max_bitrate):
        logging.info('Output directory: {directory_name}'.format(directory_name=directory_out_mp4))

        ffmpeg = config.get('Work', 'ffmpeg')
        max_files_concatenate = int(config.get('Work', 'max_files_concatenate'))
        min_fragment_duration = Decimal(config.get('Work', 'min_fragment_duration'))
        gop_size = int(config.get('Work', 'gop_size'))

        if not os.path.isdir(tmp_path):
            os.makedirs(tmp_path)

        logging.info('Temp directory: {directory_name}'.format(directory_name=tmp_path))

        scenes = []

        file_path = os.path.join(directory_in, bitrate['bitrate'])

        # checking file existence
        for fn in root.iter('File'):
            full_name = os.path.join(file_path, fn.text.replace('\\', '/'))
            if not os.path.exists(full_name):
                logging.info("File not found: " + full_name + ", searching ...")
                fn = get_nearest_file(full_name)
                if fn:
                    logging.info("File found: " + fn)
                else:
                    msg = 'Source file does not exist: {file_name}'.format(file_name=full_name)
                    raise CustomExceptiom(msg)

        i = 0

        for n in root.iter('FfmpegScene'):

            i += 1
            scene_file_name = os.path.join(tmp_path, "scene%05d" % i + ".ts")

            if os.path.exists(scene_file_name):
                os.remove(scene_file_name)

            type = n.find('Type').text
            file_name = n.find('FileName').text.replace('\\', '/')
            full_name = os.path.join(file_path, file_name)

            full_name = get_nearest_file(full_name)

            if not os.path.exists(full_name):
                msg = 'Source file does not exist: {file_name}'.format(file_name=full_name)
                raise CustomExceptiom(msg)

            if type == 'B':

                # <FfmpegScene>
                #     <Type>B</Type>
                #     <FileName>201503101800.ts</FileName>
                #     <GopFrames>7425,7500</GopFrames>
                #     <GopFile>1</GopFile>
                #     <CutStart>1.36</CutStart>
                #     <Delay>0.062</Delay>
                # </FfmpegScene>

                gop_frames = n.find('GopFrames').text
                gop_file = n.find('GopFile').text
                cut_start = n.find('CutStart').text
                delay = n.find('Delay').text

                if Decimal(cut_start) + Decimal(delay) > (gop_size - min_fragment_duration):
                    logging.info("Ignoring fragment {fragment} (B), too small: {duration}".format(fragment=i, duration=Decimal(cut_start) + Decimal(delay)))
                    continue

                # ffmpeg -i ../1200/201503101745.ts -c copy -f segment -reset_timestamps 1
                # -segment_frames 75,150 -map 0 test%0d.ts -y

                cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                      " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + \
                      " -map 0 " + tmp_path + "/test%0d.ts -y"
                cmd += " && "

                cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + \
                       " -ss " + str(Decimal(cut_start) + Decimal(delay)) + " " + bitrate['params'] + " " + \
                       scene_file_name + " -y"

                os_run_command(cmd)

            elif type == 'F':

                # <FfmpegScene>
                #     <Type>F</Type>
                #     <FileName>201503101805.ts</FileName>
                # </FfmpegScene>

                copy(full_name, scene_file_name)

            elif type == 'G':

                # <FfmpegScene>
                #     <Type>G</Type>
                #     <FileName>201503101825.ts</FileName>
                #     <GopFrames>1275,7500</GopFrames>
                #     <GopFile>1</GopFile>
                # </FfmpegScene>

                gop_frames = n.find('GopFrames').text
                gop_file = n.find('GopFile').text

                cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                      " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + \
                      " -map 0 " + tmp_path + "/test%0d.ts -y"
                cmd += " && "
                cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + \
                       " -c copy " + scene_file_name + " -y"

                os_run_command(cmd)

            elif type == 'A':

                # <FfmpegScene>
                #     <Type>A</Type>
                #     <FileName>201503101820.ts</FileName>
                #     <GopFrames>4875,4950</GopFrames>
                #     <GopFile>1</GopFile>
                #     <CutEnd>1.72</CutEnd>
                #     <Delay>0.060</Delay>
                # </FfmpegScene>

                gop_frames = n.find('GopFrames').text
                gop_file = n.find('GopFile').text
                cut_end = n.find('CutEnd').text
                delay = n.find('Delay').text

                if Decimal(cut_end) < min_fragment_duration:
                    logging.info("Ignoring fragment {fragment} (A), too small: {duration}".format(fragment=i, duration=Decimal(cut_end)))
                    continue

                cmd = "nice -n 20 " + ffmpeg + " -i " + full_name + \
                      " -c copy -f segment -reset_timestamps 1 -segment_frames " + gop_frames + \
                      " -map 0 " + tmp_path + "/test%0d.ts -y"
                cmd += " && "

                cmd += "nice -n 20 " + ffmpeg + " -i " + tmp_path + "/test" + gop_file + ".ts " + \
                       "-ss " + delay + " -t " + cut_end + " " + bitrate['params'] + " " + scene_file_name + " -y"

                os_run_command(cmd)

            scenes.append(scene_file_name)

        chunks_info = chunks(range(1, len(scenes) + 1), max_files_concatenate)
        parts_count = len(chunks_info)
        logging.info("Parts count: {parts_count}".format(parts_count=parts_count))

        if parts_count > 1:

            for i in range(0, parts_count):
                cmd = "nice -n 20 " + ffmpeg + " -i \"concat:"
                cmd += '|'.join(scenes[x - 1] for x in chunks_info[i]) + "\" "
                part_out = os.path.join(tmp_path, "part" + str(i + 1) + ".ts")

                if os.path.exists(part_out):
                    os.remove(part_out)

                cmd += " -c copy " + part_out + " -y"
                os_run_command(cmd)

                if os.path.exists(part_out):
                    logging.info(part_out + " is OK")
                else:
                    logging.error(part_out + " does not exist")

            # JOINING through Concat protocol
            cmd = "nice -n 20 " + ffmpeg + " -i \"concat:"
            cmd += '|'.join(os.path.join(tmp_path, 'part' + str(x) + '.ts') for x in range(1, parts_count + 1)) + "\" "

        else:

            cmd = "nice -n 20 " + ffmpeg + " -i \"concat:"
            cmd += '|'.join(scenes) + "\" "

        ffmpeg_out = os.path.join(tmp_path, bitrate['bitrate'] + "_ffmpeg.ts")
        mp4_tmp_out = os.path.join(tmp_path, bitrate['bitrate'] + ".mp4")
        ts_tmp_out = os.path.join(tmp_path, bitrate['bitrate'] + ".ts")
        cmd += " -c copy " + ffmpeg_out + " -y && " + ffmpeg + " -i " + ffmpeg_out + " -c copy " + ts_tmp_out + " -y " + " && rm -f " + ffmpeg_out
        os_run_command(cmd)

        if bitrate['bitrate']==bitrates_wmv:

            wmv_repl_name = root.find('WmvReplaceName').text + ".ts"
            logging.info("WMV replace name: {wmv_repl_name}".format(wmv_repl_name=wmv_repl_name))

            if not os.path.exists(ts_tmp_out):
                msg = 'Output file does not exist: {file_name}'.format(file_name=ts_tmp_out)
                logging.error(msg)
                return

            md5hash = md5_for_file_name(ts_tmp_out)
            file_size = os.path.getsize(ts_tmp_out)
            logging.info("{file}: MD5: {md5}, size: {size}".format(file=ts_tmp_out, md5=md5hash, size=file_size))
            output_files.append({'from': ts_tmp_out, 'to': os.path.join(directory_out_wmv, wmv_repl_name),
                                 'md5': md5hash,
                                 'size': file_size,
                                 'out_for_screenshots': None, 'in_for_screenshots': None})

        mp4box = config.get('Work', 'mp4box')
        cmd = mp4box + " -add " + ts_tmp_out + " " + mp4_tmp_out
        mp4box_ok = os_run_command(cmd)
        logging.info('mp4box result: ' + ('ok' if mp4box_ok else 'error'))

        if not mp4box_ok or not os.path.exists(mp4_tmp_out):

            logging.error('MP4Box conversion failed, output file does not exist: {file_name}, trying FFMpeg'.format(file_name=mp4_tmp_out))

            if os.path.exists(mp4_tmp_out):
                mp4_tmp_file_size = os.path.getsize(mp4_tmp_out)
                os.remove(mp4_tmp_out)
                logging.info('tmp file {}, size {}b removed: '.format(mp4_tmp_out, mp4_tmp_file_size))

            cmd = "nice -n 20 {ffmpeg} -i {input_file} -c copy -bsf:a aac_adtstoasc {output_file} -y".format(ffmpeg=ffmpeg, input_file=ts_tmp_out, output_file=mp4_tmp_out)
            os_run_command(cmd)

            if not os.path.exists(mp4_tmp_out):
                logging.error('FFMpeg conversion failed, output file does not exist: {file_name}'.format(file_name=mp4_tmp_out))
                return

        md5hash = md5_for_file_name(mp4_tmp_out)
        file_size = os.path.getsize(mp4_tmp_out)
        logging.info("{file}: MD5: {md5}, size: {size}".format(file=mp4_tmp_out, md5=md5hash, size=file_size))

        if 'md5-' in output_file_name:
            output_file_name = output_file_name.replace("md5", md5hash)

        f = {'from': mp4_tmp_out, 'to': os.path.join(directory_out_mp4, output_file_name),
             'md5': md5hash, 'size': file_size}

        logging.info("max_bitrate: " + max_bitrate)
        is_file_for_screenshots = bitrate['bitrate'] == max_bitrate
        if is_file_for_screenshots:
            video_for_screenshots_dir = config.get('Screenshots', 'output_dir')
            scr_input_dir = config.get('Screenshots', 'scr_input_dir')
            f['out_for_screenshots'] = os.path.join(video_for_screenshots_dir, md5hash + '.mp4')
            f['in_for_screenshots'] = os.path.join(scr_input_dir, md5hash + '.mp4')
        else:
            f['out_for_screenshots'] = None
            f['in_for_screenshots'] = None

        output_files.append(f)

        # deleting temporary files
        for x in scenes:
            if os.path.exists(x):
                os.remove(x)

        for x in range(1, 4):
            fname = tmp_path + "/test{0}.ts".format(x)
            if os.path.exists(fname):
                os.remove(fname)

        for x in range(1, parts_count + 1):
            fname = tmp_path + "/part{0}.ts".format(x)
            if os.path.exists(fname):
                os.remove(fname)

    try:
        update_task_status(id, 'executing')

        # print 'Processing task ID: {id}'.format(id=id)

        max_bitrate = str(max([int(y) for y in bitrates_mp4.split(',')]))
        logging.info('Bitrates: {bitrates}'.format(bitrates=bitrates_mp4))

        for bitrate in bitrates_mp4.split(','):
            logging.info('Processing bitrate: {bitrate}'.format(bitrate=bitrate))

            print 'Processing bitrate: {bitrate}'.format(bitrate=bitrate)

            bitrates_dict = build_dict([x for x in bitrates if x['format']==format], key="bitrate")
            b = bitrates_dict[bitrate]

            output_file_name = 'md5-{duration}-{bitrate_id}-{air_time}_replace_{media_object_id}.mp4' \
                .format(duration=duration, air_time=air_time, media_object_id=media_object_id,
                        bitrate_id=b['bitrate'])
            process_one_bitrate(b, output_file_name, max_bitrate)

        for f in output_files:

            try:
                v, a = stream_duration(f['from'])
                logging.info("File {from_file} duration: {duration}".format(from_file=f['from'], duration=v))
            except:
                msg = 'WARNING, cannot calculate video duration from file {file}'.format(file=f['from'])
                logging.error(msg)
                send_alert(msg)
                continue

            tmp_file = f['to'] + '.tmp'
            logging.info("Moving from {from_file} to temporary file {to_file}".format(from_file=f['from'], to_file=tmp_file))
            shutil.move(f['from'], tmp_file)
            logging.info("File moved")

            success = False
            err_msg = ""

            if os.path.exists(tmp_file):

                if f['size'] == os.path.getsize(tmp_file):

                    logging.info("File size OK, checking temporary file MD5")
                    fopen = open(tmp_file, 'rb')
                    md5hash = md5_for_file(fopen)
                    logging.info("MD5 hash: {md5hash}".format(md5hash=md5hash))
                    if md5hash==f['md5']:

                        # screenshots
                        if f['out_for_screenshots']:
                            os.link(tmp_file, f['out_for_screenshots'])
                            logging.info("Link created {ffrom} -> {fto}".format(ffrom=tmp_file, fto=f['in_for_screenshots']))

                            tasks_url = config.get('Screenshots', 'tasks_url')
                            api_key = config.get('Screenshots', 'api_key')

                            # curl -X POST -H "Content-Type: application/json" -H 'X-Api-Key: xxx'
                            #     --data '{ "input_file": "/mnt/videos/80095c3423709a379e6d108c1b7358ab_123.mkv" }'
                            #  'http://xxx/api/v1/tasks/'

                            # {
                            #     "input_file": "/mnt/storages/80095c3423709a379e6d108c1b7358ab_123.mkv",
                            #     "params": {
                            #         "output_format": "png",
                            #         "start_offset": "10%",
                            #         "end_offset": "10%",
                            #         "cropdetect": "1",
                            #         "graph_name": "20170501"
                            #     }
                            # }

                            # title
                            # s - sport
                            # n - news
                            # f - movies

                            try:

                                payload = {'input_file': f['in_for_screenshots']}

                                params = {}
                                if "s_" in title:
                                    params['start_offset'] = "5%"
                                    params['end_offset'] = "80%"
                                if "n_" in title:
                                    if int(duration) > 5:
                                        params['start_offset'] = "120"
                                        params['end_offset'] = str(int(duration) * 60 - 240)
                                if "f_" in title:
                                    params['start_offset'] = "15%"
                                    params['end_offset'] = "15%"

                                payload['params'] = params

                                request = urllib2.Request(tasks_url, json.dumps(payload))
                                request.add_header('X-Api-Key', api_key)
                                request.add_header('Content-Type', 'application/json')

                                response = urllib2.urlopen(request)
                                data = response.read()
                                logging.info("Task uploaded")
                                logging.info(data)

                            except urllib2.HTTPError, error:
                                logging.error("HTTPError" + str(error))
                            except Exception as error:
                                logging.error("Error" + str(error))

                        logging.info("MD5 hash is OK, renaming to {f_to}".format(f_to=f['to']))
                        os.rename(tmp_file, f['to'])
                        logging.info("File successfully renamed")


                        success = True
                    else:
                        err_msg = "MD5 hash mismatch"

                else:
                    err_msg = "File size mismatch, source size: {source_size}, moved file size: {moved_size}".\
                                  format(source_size=f['size'], moved_size=os.path.getsize(tmp_file))
            else:
                err_msg = "File does not exist"

            if not success:
                logging.error(err_msg)
                update_task_status_error(id, "Error while generating output file: {err_msg}".format(err_msg=err_msg))
                rmtree(tmp_path)
                send_alert(err_msg)
                return


        logging.info('Output file OK, saving task status...')
        update_task_status(id, 'completed')

        duration = time.time() - start
        update_task_duration(id, duration)
        logging.info('Task completed')
        logging.info('Time elapsed: {duration}'.format(duration=round(duration, 3)))

        rmtree(tmp_path)

        print 'Task completed'

    except:  # catch *all* exceptions
        e = sys.exc_info()
        msg = 'Error: %s %s % s' % (e[0], e[1], e[2])

        logging.error(msg)
        update_task_status_error(id, msg)
        rmtree(tmp_path)
        send_alert(msg)

def build_dict(seq, key):
    return dict((d[key], dict(d, index=i)) for (i, d) in enumerate(seq))


def process_active_tasks():
    global conn
    # Connecting to PG server
    try:
        conn = psycopg2.connect(connection_string)
    except Exception as e:
        logging.error("Error connecting to db " + format(e))
        sys.exit()

    global cur
    cur = conn.cursor()
    cur.execute("select t.id, t.media_object_id, t.content, s.directory_in, s.directory_out_mp4, "
                "s.directory_out_wmv, s.bitrates_mp4, s.bitrates_wmv, t.source_code, s.format \
        from tasks t \
        left join sources s on t.source_code=s.code \
        where t.status='new'")

    rows = cur.fetchall()

    if len(rows) > 0:
        logging.info('Task(s) found: {task_count}'.format(task_count=len(rows)))

        for row in rows:
            process_task(row)

    cur.close()
    conn.close()

# Starting....
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, 'tasks.cfg')

try:
    config = ConfigParser.ConfigParser()
    config.read(config_file)
except:
    print "Cannot load " + config_file
    sys.exit()

log_path = config.get('Logging', 'path')
if not log_path:
    log_path = current_path

log_file = os.path.join(log_path, 'mtvl-publ-process-tasks.log')
logging = create_timed_rotating_log(log_file)

logging.info('Task started')

# encoding settings for ffmpeg
bitrates = []

# HD
# -fflags '+igndts+genpts' \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=480:270" \
# -b:v 400k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 800k -bufsize 800k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 48k -ar 44100 -ac 1 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/400_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=640:360" \
# -b:v 900k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 1800k -bufsize 1800k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 96k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/800_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=960:540" \
# -b:v 1600k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 3200k -bufsize 3200k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 128k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/1200_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=960:540" \
# -b:v 2600k -threads 8 -r 25 -g 50 -sc_threshold 0 -maxrate 5200k -bufsize 5200k \
# -map 0:a:1 -c:a libvo_aacenc -b:a 160k -ar 44100 -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH411/2500_.m3u8 \

ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v \"yadif,scale={0}\" " \
               "-b:v {1}k -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:a:0 -c:a libfdk_aac -b:a {4}k -ac {3}"

bitrates.append({'bitrate': '400', 'format': 'HD', 'params': ffmpegParams.format("480:270", "400", "800", "1", "48")})
bitrates.append({'bitrate': '800', 'format': 'HD', 'params': ffmpegParams.format("640:360", "900", "1800", "2", "96")})
bitrates.append({'bitrate': '1200', 'format': 'HD', 'params': ffmpegParams.format("960:540", "1600", "3200", "2", "128")})
bitrates.append({'bitrate': '2500', 'format': 'HD', 'params': ffmpegParams.format("960:540", "2600", "5200", "2", "160")})

# SD
# -fflags '+igndts+genpts' \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=480:270" \
# -b:v 400K -preset medium -threads 2 -r 25 -g 50 -sc_threshold 0 -maxrate 800k -bufsize 800k \
# -map 0:a:0 -c:a libvo_aacenc -b:a 48K -ac 1 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH453/400_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=720:404" \
# -b:v 900K -preset medium -threads 2 -r 25 -g 50 -sc_threshold 0 -maxrate 1800k -bufsize 1800k \
# -map 0:a:0 -c:a libvo_aacenc -b:a 96K -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH453/800_.m3u8 \
# -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v "yadif,scale=720:404" \
# -b:v 1600K -preset medium -threads 2 -r 25 -g 50 -sc_threshold 0 -maxrate 3200k -bufsize 3200k \
# -map 0:a:0 -c:a libvo_aacenc -b:a 128K -ac 2 \
# -hls_time 10 -hls_list_size 3 -hls_wrap 4 /var/www/ram/tmp/CH453/1200_.m3u8 \

ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v \"yadif,scale={0}\" " \
               "-b:v {1}k -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:a:0 -c:a libfdk_aac -b:a {4}k -ac {3}"

bitrates.append({'bitrate': '400', 'format': 'SD', 'params': ffmpegParams.format("480:270", "400", "800", "1", "48")})
bitrates.append({'bitrate': '800', 'format': 'SD', 'params': ffmpegParams.format("720:404", "900", "1800", "2", "96")})
bitrates.append({'bitrate': '1200', 'format': 'SD', 'params': ffmpegParams.format("720:404", "1600", "3200", "2", "128")})

# XD
# -map 0:v -pix_fmt yuv420p -bsf:v h264_mp4toannexb -filter:v 'yadif,crop=720:482:0:4' \
#     -c:v libx264 -b:v 900K -preset fast -threads 4 -r 30 -g 60 -sc_threshold 0 -maxrate 1800k -bufsize 1800k \
#     -map 0:a:0 -filter:a "volume=20dB" -c:a aac -b:a 64K -ar 44100 -ac 1 \

ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -pix_fmt yuv420p -bsf:v h264_mp4toannexb -filter:v \"yadif,crop=720:482:0:4\" " \
                "-c:v libx264 -b:v 900K -preset fast -threads 4 -r 30 -g 60 -sc_threshold 0 -maxrate 1800k -bufsize 1800k " \
                "-map 0:a:0 -filter:a \"volume=20dB\" -c:a libfdk_aac -b:a 64K -ac 1 "

bitrates.append({'bitrate': '800', 'format': 'XD', 'params': ffmpegParams})


# HD1 - HD with the same audio parameters for all video bitrates
ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -aspect 16:9 " \
               "-b:v {1}k -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:a:0 -c:a copy"

bitrates.append({'bitrate': '400', 'format': 'HD1', 'params': ffmpegParams.format("480:270", "400", "800", "2", "128")})
bitrates.append({'bitrate': '800', 'format': 'HD1', 'params': ffmpegParams.format("640:360", "900", "1800", "2", "128")})
bitrates.append({'bitrate': '1200', 'format': 'HD1', 'params': ffmpegParams.format("960:540", "1600", "3200", "2", "128")})


ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -bsf:v h264_mp4toannexb -aspect 16:9 -filter:v \"yadif,scale={0}\" " \
               "-b:v {1}k -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k -map 0:a:0 -c:a copy"

bitrates.append({'bitrate': '1200', 'format': 'HD2', 'params': ffmpegParams.format("640:360", "1000", "1000", "2", "96")})
bitrates.append({'bitrate': '2500', 'format': 'HD2', 'params': ffmpegParams.format("1280:720", "2400", "2400", "2", "96")})


# ================================================================================
# SD1 - the same as reguler SD but with same audio format for all bitrates
#     -map 0:v -s 480x270 -aspect 16:9 -c:v libx264 -b:v 400K -preset medium -profile high -threads 4 -r 25 -g 50 -sc_threshold 0 -maxrate 800K -bufsize 800K \
#     -map 0:a:0 -c:a libfdk_aac -b:a 128K -ac 2 \
# ================================================================================

ffmpegParams = "-fflags \"+genpts+igndts\" -map 0:v -c:v libx264 -aspect 16:9 " \
               "-b:v {1}k -preset medium -profile high -threads 8 -g 50 -sc_threshold 0 -maxrate {2}k -bufsize {2}k " \
               "-map 0:a:0 -c:a libfdk_aac -b:a {4}k -ac {3}"

bitrates.append({'bitrate': '400', 'format': 'SD1', 'params': ffmpegParams.format("480:270", "400", "800", "2", "128")})
bitrates.append({'bitrate': '800', 'format': 'SD1', 'params': ffmpegParams.format("720:404", "900", "1800", "2", "128")})
bitrates.append({'bitrate': '1200', 'format': 'SD1', 'params': ffmpegParams.format("720:404", "1600", "3200", "2", "128")})

logging.info('Config loaded')

connection_string = get_connection_string(config)

rt = RepeatedTimer(5, process_active_tasks)
Vb